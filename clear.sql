drop table if exists
data_przydatnosci,
dostawcy,
dostepnosc_produktow,
historia_cen,
historia_sprzedazy,
historia_zatrudnien,
kategorie,
klienci,
placowki,
pracownicy,
premie,
produkty,
promocje,
rabaty_klienckie,
reklamacje_klientow,
stanowiska,
zakupy,
produkty_dostawcy
cascade;

drop function if exists tel() cascade;
drop function if exists daty() cascade;
drop function if exists daty2() cascade;
drop function if exists dt() cascade;
drop function if exists dt2() cascade;
drop function if exists nl() cascade;
drop function if exists widelki() cascade;
drop function if exists braki(int) cascade;
drop function if exists dostawcy_produktu(varchar) cascade;
drop function if exists kariera_zawodowa(int) cascade;
drop function if exists kariera_zawodowa2(varchar,varchar) cascade;
drop function if exists pracownicy_placowki(int) cascade;
drop function if exists pracownicy_placowki2(int) cascade;
drop function if exists produkt_cena(int,int) cascade;
drop function if exists produkty_promocyjne() cascade;
drop function if exists stali_klienci() cascade;
drop function if exists zakupy_klienta(int) cascade;
