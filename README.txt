﻿PROJEKT:
BardzoDuży GigaMarket, czyli sieć sklepów

WYKONAWCY:
Mateusz Tokarz, Gabriela Czarska, Dawid Rączka, Bruno Pitrus

APLIKACJA:
Powinna się skompilować i uruchomić pod aktualną wersją IntelliJ IDEA. Na razie w wersji pre-alpha, demozapytanie wyświetla wszystkie produkty będące w kategorii której nazwę się wpisze. Połączenie obecnie zahardkodowane na localhost i konto postgres, nazwa bazy sklep. Można zmienić jedynie przed kompilacją w pliku ZapytaniaSQL.java.