import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Bruno Pitrus on 26.06.2016.
 */
public class PracownicyOkn
{
	JPanel panPracownicy;
	private JComboBox comPlacówki;
	private JButton butView;
	private JButton butUsun;
	private JButton butDodaj;
	private JTable tabResults;
	static final String[] kolumny={"id","imię","nazwisko"};
	PracownicyOkn()
	{
		try
		{
			ResultSet rs=ZapytaniaSQL.placowkiSelector.executeQuery();
			DefaultComboBoxModel<ZapytaniaSQL.IdNazwa>mod=new DefaultComboBoxModel<>();
			mod.addElement(new ZapytaniaSQL.IdNazwa("Wszystkie placówki"));
			while(rs.next())
				mod.addElement(new ZapytaniaSQL.IdNazwa(rs.getInt("id_placowki"),rs.getString("adres")));
			comPlacówki.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
		comPlacówki.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				odswiezOkno();
			}
		});
		butUsun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Integer id=(Integer)(tabResults.getValueAt(tabResults.getSelectedRow(),0));
				try
				{
					ZapytaniaSQL.usunPrac.setInt(1, id);
					ZapytaniaSQL.usunPrac.execute();
				}catch(SQLException ee)
				{
					ee.printStackTrace();
				}
				odswiezOkno();
			}
		});
		butDodaj.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DodajPrac.main();
				odswiezOkno();
			}
		});
		butView.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Integer id=(Integer)(tabResults.getValueAt(tabResults.getSelectedRow(),0));
				ViewPrac.main(id);
			}
		});
		odswiezOkno();
	}
	public void odswiezOkno()
	{
		PreparedStatement stat;
		final ZapytaniaSQL.IdNazwa c=(ZapytaniaSQL.IdNazwa)comPlacówki.getSelectedItem();
		try
		{
			if (c.id == null)
			{
				stat=ZapytaniaSQL.pracownicyAll;
			}
			else
			{
				stat = ZapytaniaSQL.pracownicySome;
				stat.setInt(1, c.id);
			}
			ResultSet rs=stat.executeQuery();
			DefaultTableModel mod=new DefaultTableModel()
			{
				@Override public boolean isCellEditable(int a,int b){return false;};
			};
			mod.setColumnIdentifiers(kolumny);
			while(rs.next())
				mod.addRow(new Object[]{rs.getInt("id_pracownika"),rs.getString("imie"),rs.getString("nazwisko")});
			tabResults.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
	}

}
