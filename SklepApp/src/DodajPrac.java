import javax.swing.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.sql.SQLException;

public class DodajPrac extends JDialog
{
	private JPanel contentPane;
	private JButton buttonOK;
	private JButton buttonCancel;
	private JTextField texId;
	private JTextField texImie;
	private JTextField texNazwisko;
	private JTextField texRach;

	public DodajPrac()
	{
		setContentPane(contentPane);
		setModal(true);
		setResizable(false);
		getRootPane().setDefaultButton(buttonOK);

		buttonOK.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onOK();}
		});

		buttonCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onCancel();}
		});

// call onCancel() when cross is clicked
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				onCancel();
			}
		});

// call onCancel() on ESCAPE
		contentPane.registerKeyboardAction(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);




	}

	private void onOK()
	{
		try
		{
			ZapytaniaSQL.dodajPrac.setInt(1,Integer.parseInt(texId.getText()));
			ZapytaniaSQL.dodajPrac.setString(2, texImie.getText());
			ZapytaniaSQL.dodajPrac.setString(3,texNazwisko.getText());
			ZapytaniaSQL.dodajPrac.setBigDecimal(4, new BigDecimal(texRach.getText()));
			ZapytaniaSQL.dodajPrac.execute();
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
// add your code here
		dispose();
	}

	private void onCancel()
	{
// add your code here if necessary
		dispose();
	}

	public static void main()
	{
		DodajPrac dialog = new DodajPrac();
		dialog.pack();
		dialog.setVisible(true);
	}
}
