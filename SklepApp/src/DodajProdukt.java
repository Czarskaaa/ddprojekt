import javax.swing.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DodajProdukt extends JDialog
{
	private JPanel contentPane;
	private JButton buttonOK;
	private JButton buttonCancel;
	private JTextField texId;
	private JTextField texNazwa;
	private JComboBox comKategorie;

	public DodajProdukt()
	{
		setContentPane(contentPane);
		setModal(true);
		setResizable(false);
		getRootPane().setDefaultButton(buttonOK);

		buttonOK.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onOK();}
		});

		buttonCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onCancel();}
		});

// call onCancel() when cross is clicked
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				onCancel();
			}
		});

// call onCancel() on ESCAPE
		contentPane.registerKeyboardAction(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


		try
		{
			ResultSet rs=ZapytaniaSQL.categorySelector.executeQuery();
			DefaultComboBoxModel<ZapytaniaSQL.IdNazwa>mod=new DefaultComboBoxModel<>();
			while(rs.next())
				mod.addElement(new ZapytaniaSQL.IdNazwa(rs.getInt("id_kategorii"),rs.getString("nazwa")));
			comKategorie.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}

	}

	private void onOK()
	{
		try
		{
			ZapytaniaSQL.dodajProdukt.setInt(1,Integer.parseInt(texId.getText()));
			ZapytaniaSQL.dodajProdukt.setString(2,texNazwa.getText());
			ZapytaniaSQL.dodajProdukt.setInt(3,((ZapytaniaSQL.IdNazwa)comKategorie.getSelectedItem()).id);
			ZapytaniaSQL.dodajProdukt.execute();
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
// add your code here
		dispose();
	}

	private void onCancel()
	{
// add your code here if necessary
		dispose();
	}

	public static void main()
	{
		DodajProdukt dialog = new DodajProdukt();
		dialog.pack();
		dialog.setVisible(true);
	}
}
