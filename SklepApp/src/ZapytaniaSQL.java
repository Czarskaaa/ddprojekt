
import java.sql.*;

/**
 * Created by Bruno Pitrus on 21.05.2016.
 */
public final class ZapytaniaSQL
{
	//Dane do łączenia z bazą można zmienić poniżej.
	static final String DB_URL = "jdbc:postgresql://localhost:5432/sklep";
	static final String DB_USER = "postgres";
	static final String DB_PASS = "";
	static Connection connection;
	//Zapytania dodajemy poniżej wedeług przykładu i tylko w tym pliku. Ułatwia to uniknięcie możliwych bugów.

	static final String categorySelector_="select id_kategorii, nazwa from kategorie";
	static PreparedStatement categorySelector;
	static final String produktyAll_="select id_produktu, nazwa from produkty";
	static PreparedStatement produktyAll;
	static final String produktySome_="select id_produktu, nazwa from produkty where id_kategorii = ?";
	static PreparedStatement produktySome;
	static final String usunProdukt_="delete from produkty where id_produktu = ?";
	static PreparedStatement usunProdukt;
	static final String dodajProdukt_="insert into produkty(id_produktu,nazwa,id_kategorii) values(?,?,?)";
	static PreparedStatement dodajProdukt;
	static final String placowkiSomeSelector_ ="select id_placowki, adres from " +
			" placowki join dostepnosc_produktow using (id_placowki)" +
			" where id_produktu = ?";
	static PreparedStatement placowkiSomeSelector;
	static final String placowkiSelector_="select id_placowki, adres from placowki";
	static PreparedStatement placowkiSelector;
	static final String produktInfo_="select produkty.nazwa pro,kategorie.nazwa ka" +
			" from produkty join kategorie using (id_kategorii)" +
			" where id_produktu = ?";
	static PreparedStatement produktInfo;
	static final String produktCena_="select produkt_cena(?,?)";
	static PreparedStatement produktCena;
	static final String promocje_="select * from produkty_promocyjne()";
	static PreparedStatement promocje;
	static final String pracownicyAll_="select id_pracownika,imie,nazwisko from pracownicy";
	static PreparedStatement pracownicyAll;
	static final String pracownicySome_="select * from pracownicy_placowki(?)";
	static PreparedStatement pracownicySome;
	static final String dodajPrac_="insert into pracownicy(id_pracownika,imie,nazwisko,nr_rachunku_bankowego) values(?,?,?,?)";
	static PreparedStatement dodajPrac;
	static final String usunPrac_="delete from pracownicy where id_pracownika = ?";
	static PreparedStatement usunPrac;
	static final String nazwiskoPrac_="select imie||' '||nazwisko from pracownicy where id_pracownika = ?";
	static PreparedStatement nazwiskoPrac;
	static final String karieraZaw_="select nazwa,id_placowki,pensja,zatrudniony_od,zatrudniony_do from kariera_zawodowa(?)";
	static PreparedStatement karieraZaw;
	static void prepareDB() throws SQLException
	{
		connection = DriverManager.
				getConnection(DB_URL, DB_USER, DB_PASS);
		//Poniżej kompilujemy każde zapytanie które zostanie uzyte.

		categorySelector=connection.prepareStatement(categorySelector_);
		produktyAll=connection.prepareStatement(produktyAll_);
		produktySome=connection.prepareStatement(produktySome_);
		usunProdukt=connection.prepareStatement(usunProdukt_);
		dodajProdukt=connection.prepareStatement(dodajProdukt_);
		placowkiSomeSelector =connection.prepareStatement(placowkiSomeSelector_);
		placowkiSelector=connection.prepareStatement(placowkiSelector_);
		produktInfo=connection.prepareStatement(produktInfo_);
		produktCena=connection.prepareStatement(produktCena_);
		promocje=connection.prepareStatement(promocje_);
		pracownicyAll=connection.prepareStatement(pracownicyAll_);
		pracownicySome=connection.prepareStatement(pracownicySome_);
		dodajPrac=connection.prepareStatement(dodajPrac_);
		usunPrac=connection.prepareStatement(usunPrac_);
		nazwiskoPrac=connection.prepareStatement(nazwiskoPrac_);
		karieraZaw=connection.prepareStatement(karieraZaw_);
	}
	static class IdNazwa//Dla selektorów.
	{
		public final Integer id;
		public final String nazwa;
		public IdNazwa(int id,String nazwa)//Wpisy prawdziwe
		{
			this.id=id;
			this.nazwa=nazwa;
		}
		public IdNazwa(String nazwa)//Wpisy fikcyjne
		{
			this.id=null;
			this.nazwa=nazwa;
		}
		@Override public String toString()
		{
			return id==null?nazwa://Jeśli wpis jest fikcyjny to nie wypisujemy jego numeru.
					"["+id+"] "+nazwa;
		}
	}
}
