import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ViewPrac extends JDialog
{
	private JPanel contentPane;
	private JButton buttonCancel;
	private JTextField texName;
	private JTable tabKz;
	final int idprac;
	static final String[] kolumny={"nazwa","# plac","pensja","zatr. od","zatr. do"};
	public ViewPrac(int id)
	{
		idprac=id;
		setContentPane(contentPane);
		setModal(true);
		try
		{
			ZapytaniaSQL.nazwiskoPrac.setInt(1,idprac);
			ResultSet rs=ZapytaniaSQL.nazwiskoPrac.executeQuery();
			rs.next();
			texName.setText("["+idprac+"] "+rs.getString(1));
			ZapytaniaSQL.karieraZaw.setInt(1,idprac);
			rs=ZapytaniaSQL.karieraZaw.executeQuery();
			DefaultTableModel mod=new DefaultTableModel()
			{
				@Override public boolean isCellEditable(int a,int b){return false;};
			};
			mod.setColumnIdentifiers(kolumny);
			while(rs.next())
				mod.addRow(new Object[]{rs.getString("nazwa"),rs.getInt("id_placowki"),rs.getInt("pensja"),rs.getDate("zatrudniony_od"),rs.getDate("zatrudniony_do")});
			tabKz.setModel(mod);

		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}

		buttonCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onCancel();}
		});

// call onCancel() when cross is clicked
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				onCancel();
			}
		});

// call onCancel() on ESCAPE
		contentPane.registerKeyboardAction(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	}

	private void onCancel()
	{
// add your code here if necessary
		dispose();
	}

	public static void main(int pracId)
	{
		ViewPrac dialog = new ViewPrac(pracId);
		dialog.pack();
		dialog.setVisible(true);
	}
}
