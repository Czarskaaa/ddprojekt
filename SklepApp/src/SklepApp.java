import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Created by Bruno Pitrus on 13.06.2016.
 */
public class SklepApp
{
	private JPanel mainPanel;
	private JTabbedPane tabbedPane1;
	private JPanel panProdukty;
	private JPanel panPracownicy;
	private static final JFrame window = new JFrame("Bardzo Duży Giga Market");

	public SklepApp() {


	}

	public static void main(String[] args){
		//Inicjalizacja bazy danych poniżej.
		try {
			ZapytaniaSQL.prepareDB();
		}
		catch (SQLException e) {//To powoduje wydrukowanie detali błędu w konsoli. łatwiej się debuguje. SQLException musisz łapać bo jest checked.
			e.printStackTrace();
		}
		//Normalne tworzenie okienka. To już znacie.
		try {

			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		}
		catch (UnsupportedLookAndFeelException|ClassNotFoundException|InstantiationException|IllegalAccessException e) {}

		window.setBounds(200, 200, 600, 600);
		//TODO: Zrobić jakiś main window. Poniżej uruchamiamy okienko z produktami to rozwiązanie tymczasowe (BP)
		window.setContentPane(new SklepApp().mainPanel);
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);
	}

	private void createUIComponents()
	{
		panProdukty=new ProduktyOkn().panProdukty;
		panPracownicy=new PracownicyOkn().panPracownicy;
	}
}
