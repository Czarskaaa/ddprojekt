import javax.swing.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ViewProdukt extends JDialog
{
	private JPanel contentPane;
	private JButton buttonCancel;
	private JTextField texCena;
	private JComboBox comPlacówki;
	private JTextField texNazwa;
	private JLabel labKateg;
	final int idprod;
	public ViewProdukt(int id)
	{
		idprod=id;
		setContentPane(contentPane);
		setModal(true);
		setResizable(false);
		try
		{
			ZapytaniaSQL.produktInfo.setInt(1,idprod);
			ResultSet rs=ZapytaniaSQL.produktInfo.executeQuery();
			rs.next();
			texNazwa.setText("["+idprod+"] "+rs.getString("pro"));
			labKateg.setText(rs.getString("ka"));
			ZapytaniaSQL.placowkiSomeSelector.setInt(1,idprod);
			rs=ZapytaniaSQL.placowkiSomeSelector.executeQuery();
			DefaultComboBoxModel<ZapytaniaSQL.IdNazwa>mod=new DefaultComboBoxModel<>();
			while(rs.next())
				mod.addElement(new ZapytaniaSQL.IdNazwa(rs.getInt("id_placowki"),rs.getString("adres")));
			comPlacówki.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
		comPlacówki.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				try
				{
					ZapytaniaSQL.produktCena.setInt(1, idprod);
					ZapytaniaSQL.produktCena.setInt(2,((ZapytaniaSQL.IdNazwa)comPlacówki.getSelectedItem()).id);
					ResultSet rs=ZapytaniaSQL.produktCena.executeQuery();
					rs.next();
					texCena.setText(rs.getString(1));
				}catch(SQLException ee)
				{
					ee.printStackTrace();
				}
			}
		});


		buttonCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {onCancel();}
		});

// call onCancel() when cross is clicked
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				onCancel();
			}
		});

// call onCancel() on ESCAPE
		contentPane.registerKeyboardAction(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	}

	private void onCancel()
	{
// add your code here if necessary
		dispose();
	}

	public static void main(int id)
	{
		ViewProdukt dialog = new ViewProdukt(id);
		dialog.pack();
		dialog.setVisible(true);
	}
}
