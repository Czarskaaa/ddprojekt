import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.*;
import java.util.Vector;

/**
 * Created by Bruno Pitrus on 20.06.2016.
 */
public class ProduktyOkn
{
	JTable tabResults;
	JComboBox<ZapytaniaSQL.IdNazwa> comKategorie;
	JPanel panProdukty;
	private JButton butDodaj;
	private JButton butView;
	private JButton butUsun;
	static final String[] kolumny={"id","nazwa"};
	ProduktyOkn()
	{
		try
		{
			ResultSet rs=ZapytaniaSQL.categorySelector.executeQuery();
			DefaultComboBoxModel<ZapytaniaSQL.IdNazwa>mod=new DefaultComboBoxModel<>();
			mod.addElement(new ZapytaniaSQL.IdNazwa("Wszystkie kategorie"));
			mod.addElement(new ZapytaniaSQL.IdNazwa("[‼] Promocje"));
			while(rs.next())
				mod.addElement(new ZapytaniaSQL.IdNazwa(rs.getInt("id_kategorii"),rs.getString("nazwa")));
			comKategorie.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
		comKategorie.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				odswiezOkno();
			}
		});
		butUsun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Integer id=(Integer)(tabResults.getValueAt(tabResults.getSelectedRow(),0));
				try
				{
					ZapytaniaSQL.usunProdukt.setInt(1, id);
					ZapytaniaSQL.usunProdukt.execute();
				}catch(SQLException ee)
				{
					ee.printStackTrace();
				}
				odswiezOkno();
			}
		});
		butDodaj.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DodajProdukt.main();
				odswiezOkno();
			}
		});
		butView.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Integer id=(Integer)(tabResults.getValueAt(tabResults.getSelectedRow(),0));
				ViewProdukt.main(id);
			}
		});
		odswiezOkno();
	}
	public void odswiezOkno()
	{
		PreparedStatement stat;
		final ZapytaniaSQL.IdNazwa c=(ZapytaniaSQL.IdNazwa)comKategorie.getSelectedItem();
		try
		{
			if (c.id == null)
			{
				if("Wszystkie kategorie".equals(c.nazwa))
				stat = ZapytaniaSQL.produktyAll;
				else
				stat=ZapytaniaSQL.promocje;
			}
			else
			{
				stat = ZapytaniaSQL.produktySome;
				stat.setInt(1, c.id);
			}
			ResultSet rs=stat.executeQuery();
			DefaultTableModel mod=new DefaultTableModel()
			{
				@Override public boolean isCellEditable(int a,int b){return false;};
			};
			mod.setColumnIdentifiers(kolumny);
			while(rs.next())
				mod.addRow(new Object[]{rs.getInt("id_produktu"),rs.getString("nazwa")});
			tabResults.setModel(mod);
		}catch(SQLException ee)
		{
			ee.printStackTrace();
		}
	}
}
